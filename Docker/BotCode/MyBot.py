import os.path
import telebot
import concurrent.futures
import config
import re
import requests
import random
import time
import mysql.connector
from datetime import datetime
from threading import Thread
from telebot import types
#
cnx = mysql.connector.connect(user=config.username, password=config.password,host='localhost',port='3306',database='bot_users')
#
bot = telebot.TeleBot(config.token)
#
@bot.message_handler(commands=['start'])
def handle_start(message):
    try:
        
        cursor = cnx.cursor()
        add_user = ("INSERT INTO users "
                "VALUES (%s, %s)")
        data_user=(message.from_user.id,str(message.from_user.username));
        cursor.execute(add_user, data_user)
        cnx.commit()
        cursor.close()
    except Exception:
        print("user already in database")
    handle_wantrandompicture(message)
    sub_chat_id=message
    log(message)
#
@bot.message_handler(commands=['help'])
def handle_help_start(message):
    handle_wantrandompicture(message)
    sub_chat_id=message
    log(message)
#
@bot.message_handler(commands=['wantrandompicture'])
def handle_wantrandompicture(message):
    photo="https://loremflickr.com/"+str(random.randrange(1024, 2048, 16))+'/'+str(random.randrange(1024, 2048, 22))
    keyboard = types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)
    button_site_link = types.KeyboardButton(text="Хочу посетить ваш сайт")
    keyboard.add(button_site_link)
    bot.send_photo(message.chat.id, photo,'Вот ваше рандомное фото', reply_markup=keyboard)
    log(message)
#
@bot.message_handler(commands=['postal2cit'])
def handle_postal2cit(message):
    photo="https://torrent-igruha.org/uploads/posts/2015-11/1448649956_ss_df69841962a068dbeb740aff9108cb5dde6b3d6c.1920x1080.jpg"
    bot.send_photo(message.chat.id, photo,'POSTAL2 FOREVER'+ '\n'+'"Guns don`t kill people, I DO!"' +'\n'+ '©Postal Dude')
    #bot.send_audio(message.chat.id, open('C:/Users/Я/Downloads/uio.mp3','rb'))
    log(message)
#
@bot.message_handler(content_types=['text'])
def handle_text(message):
    if(message.text=='Хочу посетить ваш сайт'):
         bot.send_document(message.chat.id, open(os.path.abspath("app/Content_Video/site.mp4"), 'rb'),caption='http://abiturientik.com')
    else:
        bot.send_photo(message.chat.id, open('app/Content_Photo/test_bot.png','rb'), caption="Я вас не понял")
    #bot.send_photo(ид_получателя, 'https://example.org/адрес/картинки.jpg');
    log(message)
#
bot.polling(none_stop=True)
#
def log(message):
    #file = open(".\\users.txt",'a')
    print("\n ------")
    print(datetime.now())
    print("Сообщение от {0} {1}. (id = {2}) \nТекст = {3}".format(message.from_user,message.from_user.last_name,str(message.from_user.id), message.text))
    file_log_message_string=str("\n ------"+"Сообщение от "+str(message.from_user)+" "+str(message.from_user.last_name)+' '+ str(message.from_user.id)+ "\nТекст = "+message.text)
#
bot.polling(none_stop=True)
